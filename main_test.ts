// import express, { NextFunction } from 'express';
// import {Request, Response} from 'express';
import express from 'express';
import bodyParser from 'body-parser';
import path from 'path';
import moment from 'moment';
import fs from 'fs';
import multer from 'multer';
import expressSession from 'express-session';
// import { count } from 'console';



const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.resolve('uploads/'));
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
})

const upload = multer({ storage });

// const upload = multer({dest: './uploads/'})  // use multer to upload file need to specify the file location to store the upoad document

const app = express();

app.use(expressSession({
    secret: 'Tecky Project1 Group8 Basketball',
    saveUninitialized: true
}));

app.use((req, res, next) => {
    console.log('[' + moment().format('yyyy-MM-DD HH:mm:ss') + ']', req.path, 'Request by: ', req.ip);
    next();
})

app.use(express.static('public'));   // 如果加打'/public'就連唔到index.css --> 但如果'public'就ok

app.use('uploads', express.static('uploads'))

app.use(bodyParser.urlencoded({ extended: false }));

// app.get('/add-postit', async(req, res) => {
//     const notes = JSON.parse (await fs.promises.readFile('notes.json', 'utf-8'))
//     console.log(req.query);
//     notes.push(req.query);
//     await fs.promises.writeFile('notes.json', JSON.stringify(notes))
//     res.send('done')       // res.send will direct you to a new address (new page)?????
// })

// app.get('/', function(req:Request, res:Response, next:NextFunction){
//     let file = path.join(__dirname, 'public', 'index.html')
//     res.sendFile(file)
// })

app.get('/teamInfo_store', async (req, res) => {
    const notes = JSON.parse(await fs.promises.readFile('notes.json', 'utf-8'))
    res.json(notes)   // res.json(notes)  <-- 話比express 知將 'notes' 用json 格式send出去
})

app.post('/login', async (req, res) => {
    console.log(req.body);
    const users = JSON.parse(await fs.promises.readFile('users.json', 'utf-8')) // to load the file
    for (const user of users) {  // for loop to check every object inside the user json array
        if (user.username === req.body.username && user.password === req.body.password) {
            if (req.session != null) {
                req.session['user'] = req.body.username     // session記錄身份  <-- 因為一進入website session就會有cookie記錄每個ip身份  --> req.session['user'] = req.body.username  --> 於session 設立不同key, 設立session user key以記錄這個cookie為register user <-- 以後也認得這user --> login 
                res.json("You are admin.")
                return;
            }
        }
    }
    res.json('unauthorized login.');
    // res.json('admin login success.');
})

app.get('/logout', (req,res) => {
    req.session.destroy(() => {
        res.redirect('/');
    }) ;

})

app.post('/add-team', upload.single('photo'), async (req, res) => {
    const notes = JSON.parse(await fs.promises.readFile('notes.json', 'utf-8'))
    console.log(req.body)
    console.log(req.file)
    console.log(req.session)
    // notes.push(req.body);
    const note = {
        content: req.body.content,
        color: req.body.color,
        file: req.file.filename,
        ip: req.ip
        // count: Number`
    }
    notes.push(note)
    let count = req.session['count'] || 0;
    count++
    req.session['count'] = count;
    console.log(count, req.method, req.url);
    // if (req.session != null) {
    //     req.session.count = req.session.count == null ? 1 : req.session.count + 1
    // }
    await fs.promises.writeFile('notes.json', JSON.stringify(notes))
    // res.send('你係第 ' + req.session['count'] + '次俾嘢嚟')       // res.send will direct you to a new address (new page)?????
    if (req.session['user']) {
        res.send( 'Hello! ' + "\' " + req.session['user'] + " \'" + " 你係第" + req.session['count'] + "次 up load.")
    } else {
        res.send('搞掂！ 你係第' + req.session['count'] + '次 up load.')
    }
    // count only effective when I upload photo(file)

})

app.use((req, res) => {
    res.sendFile(path.resolve('./public/404.html'));
})

const PORT = 8080;

app.listen(PORT, () => {
    console.log(`Listen at http://localhost:${PORT}/`);

})