create database basketball_DB;

\c basketball_DB;


create TABLE users (
    id serial primary key,
    username VARCHAR(255),
    password VARCHAR(255),
    created_at TIMESTAMP,
    updated_at TIMESTAMP,
    email varchar(255),
    google_access_token varchar(255) unique,
    role VARCHAR(20),
    user_icon varchar(500)
);

CREATE table Matches (
    id SERIAL primary key,
    match_day INTEGER not null, 
    match_month INTEGER not null,
    match_time VARCHAR(255),
    match_home_team_name VARCHAR(255),
    match_home_team_score INTEGER not null,
    match_away_team_name VARCHAR(255),
    match_away_team_score INTEGER not null,
    match_id INTEGER not null,
    match_venus VARCHAR(255)
);

CREATE table NBAMatch (
    id SERIAL primary key,
    PLAYERID INTEGER not null, 
    PLAYERFULLNAME VARCHAR(255),
    OWNTEAM VARCHAR(255),
    PTS INTEGER not null,
    TOT INTEGER not null,
    A INTEGER not null
);

CREATE table Team (
    id SERIAL primary key,
    name varchar(255),
    icon varchar(255),
    brand_story varchar(1000),
    date_of_establishment INTEGER
);

CREATE table Player (
    id SERIAL primary key,
    team_id INTEGER not null,
    name_ch varchar(255),
    name_en varchar(255),
    position varchar(255),
    dateOfBirth date,
    height INTEGER,
    weight INTEGER,
    photo varchar(255)
);

CREATE table Coach (
    id SERIAL primary key,
    team_id INTEGER,
    name_ch varchar(255),
    name_en varchar(255),
    position varchar(255),
    photo varchar(255)
);

CREATE table Team_Ranking (
    id SERIAL primary key,
    team_id INTEGER not null,
    season INTEGER not null,
    number_of_match INTEGER not null,
    win varchar(255) not null,
    lose varchar(255) not null,
    percentage INTEGER not null

);


\d ;

-- insert into users (username,password) values
-- ('alice','wong');

-- alter table users
-- add COLUMN role VARCHAR(20);

-- alter table users add column user_icon varchar(500);

alter table users alter column email type varchar(255);

-- drop table match;
-- CREATE table Matches (
--     id SERIAL primary key,
--     match_day INTEGER not null, 
--     match_month INTEGER not null,
--     match_time VARCHAR(255),
--     match_home_team_name VARCHAR(255),
--     match_home_team_score INTEGER not null,
--     match_away_team_name VARCHAR(255),
--     match_away_team_score INTEGER not null,
--     match_id INTEGER not null,
--     match_venus VARCHAR(255)
-- );
