import { Client } from 'pg'
import { env } from './env'

export const db = new Client({
  database: env.DB_NAME,
  user: env.DB_USER,
  password: env.DB_PASS,
})

db.connect();