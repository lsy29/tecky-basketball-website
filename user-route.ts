import fetch from 'node-fetch'
import express, { Request, Response } from 'express'
import { db } from './db'
// import { isLoggedIn } from './guards';
// import { hashPassword, checkPassword } from './hash'

// type User = {
//   username: string
//   password: string
// }

export let userRoute = express.Router()

userRoute.post('/logout', logout)
// userRoute.post('/login', login)
// userRoute.post('/login-form', login)
// userRoute.post('/register', register)
userRoute.get('/login/google', loginGoogle)
userRoute.get('/role', getRole)
// userRoute.get('/admin-form', isAdmin)



function logout(req: Request, res: Response) {
  req.session['user'] = false
  res.status(200).json('ok')
}


type GoogleResult = {
  id: string
  email: string
  verified_email: boolean
  name: string
  given_name: string
  family_name: string
  picture: string
  locale: string
  hd: string
}
async function loginGoogle(req: express.Request, res: express.Response) {
  const accessToken = req.session?.['grant'].response.access_token
  const fetchRes = await fetch(
    'https://www.googleapis.com/oauth2/v2/userinfo',
    {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    },
  )
  const fetchResult: GoogleResult = await fetchRes.json()
  let dbResult = await db.query(`select * from users where email = $1`, [
    fetchResult.email,
  ])
  let user = dbResult.rows[0]

  // if this user is new comer
  if (!user) {
    dbResult = await db.query(
      `insert into users (email, google_access_token) values ($1,$2) returning *`,
      [fetchResult.email, accessToken],
    )
    req.session['user'] = dbResult.rows[0]
    res.redirect('/')
    return
  }

  // if this user already registered
  await db.query(`update users set google_access_token = $1 where id = $2`, [
    accessToken,
    user.id,
  ])
  user.google_access_token = accessToken
  delete user.password
  req.session['user'] = user
  res.redirect('/')
}

function getRole(req: Request, res: Response) {
  console.log('get role: ', req.session['user'].role, ", user: ", req.session['user']);

  if (!req.session || !req.session['user']) {
    res.send('guest')
    //res.redirect('/');
    return
  }
  if (req.session['user'].role === 'admin') {
    res.send('admin')
    //res.redirect('/admin-form');
    return
  }
  res.end('normal')
}
