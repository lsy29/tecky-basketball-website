import express from "express";
import expressSession from "express-session";
import path from "path";
import { format } from "fecha";
import dotenv from "dotenv";
import http from "http";
import { env } from "./env";
import { db } from "./db";
import socketIO from "socket.io";
import { setSocketIO } from "./socketio";
import grant from "grant";
import { userRoute } from "./user-route";
// import bodyParser from "body-parser";
import multer from "multer";
import bcrypt from "bcryptjs";
// import { checkPassword, hashPassword } from './hash';
// import fetch from 'node-fetch';
import { isLoggedIn } from "./guards";
// import fs from "fs";
// import XLSX from "xlsx";

type Match = {
  id: number;
  match_day: number;
  match_month: number;
  match_time: string;
  match_home_team_name: string;
  match_home_team_score: number;
  match_away_team_name: string;
  match_away_team_score: number;
  match_id: string;
  match_venus: string;
};

// type NBAMatch = {
//   PLAYERID: number;
//   PLAYERFULLNAME: string;
//   OWNTEAM: string;
//   PTS: number;
//   TOT: number;
//   A: number;
// };

dotenv.config();

let app = express();
let server = new http.Server(app);
let io = new socketIO.Server(server);
setSocketIO(io);

let sessionMiddleware = expressSession({
  secret: "secret" as string,
  resave: true,
  saveUninitialized: true,
});
app.use(sessionMiddleware);

const grantExpress = grant.express({
  defaults: {
    origin: `http://${env.HOST}:${env.PORT}`,
    transport: "session",
    state: true,
  },
  google: {
    key: env.GOOGLE_CLIENT_ID || "",
    secret: env.GOOGLE_CLIENT_SECRET || "",
    scope: ["profile", "email"],
    callback: "/login/google",
  },
});

let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./uploads");
  },
  filename: (req, file, cb) => {
    let filename = file.originalname;
    cb(null, filename);
  },
});

dotenv.config();

const upload = multer({ storage });

app.use(grantExpress as express.RequestHandler);
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("uploads", express.static("uploads"));
app.use(express.static("public"));
app.use(userRoute);

app.use((req, res, next) => {
  let counter = req.session["counter"] || 0;
  counter++;
  req.session["counter"] = counter;
  // console.log(counter);
  next();
});

app.get("/user-profile", isLoggedIn, async (req, res) => {
  let userinfo = await db.query(`SELECT username, email, user_icon FROM users WHERE users.id = $1`, [
    req.session["user"].id,
  ]);
  console.log(userinfo.rows[0]);
  res.status(200).json(userinfo.rows[0]);
});

app.post("/userprofile-register-username", upload.any(), async (req, res) => {
  console.log("req.body.username: ", req.body);
  // console.log("req.body.email: ", req.body.email);

  let userInfo = req.body.username;

  console.log("userInfo: ", userInfo);

  let result = await db.query(
    // `INSERT INTO users (username) VALUES ($1)`,
    // [userInfo.username]
    `UPDATE users set username = $1 WHERE id = $2`,
    [userInfo, req.session["user"].id]
  );

  console.log(result);

  res.status(200).end("usernameUpdated");
});

app.post("/userprofile-register-email", upload.any(), async (req, res) => {
  console.log("req.body.email", req.body);

  let userInfo = req.body.email;

  console.log("userInfo: ", userInfo);

  let result = await db.query(
    // `INSERT INTO users (email) VALUES ($1)`,
    // [userInfo.email]
    `UPDATE users set email = $1 WHERE id = $2`,
    [userInfo, req.session["user"].id]
  );

  console.log(result);

  res.status(200).end("emailUpdated");
});

// app.get("/userprofile-form", isLoggedIn, async (req, res) => {
//   let userRegisterInfo = await db.query(`SELECT username, email FROM users WHERE users.id = $1`, [req.session['user'].id])
//   console.log(userRegisterInfo.rows[0]);
//   res.status(200).end("userInfo-sent");

// })

app.post("/matches", upload.single("image"), async (req, res) => {
  let match_day = req.body.match_day;
  let match_month = req.body.match_month;
  let match_time = req.body.match_time;
  let match_home_team_name = req.body.match_home_team_name;
  let match_home_team_score = req.body.match_home_team_score;
  let match_away_team_name = req.body.match_away_team_name;
  let match_away_team_score = req.body.match_away_team_score;
  let match_id = req.body.match_id;
  let match_venus = req.body.match_venus;
  try {
    await db.query(
      /*sql*/ `insert into matches (match_day,match_month,match_time,match_home_team_name,match_home_team_score,match_away_team_name,match_away_team_score,match_id,match_venus) values ($1,$2,$3,$4,$5,$6,$7,$8,$9)`,
      [
        match_day,
        match_month,
        match_time,
        match_home_team_name,
        match_home_team_score,
        match_away_team_name,
        match_away_team_score,
        match_id,
        match_venus,
      ]
    );
    res.status(201).end("created");
  } catch (error) {
    console.log("failed to insert match:", error);
    res.status(500).end("failed to insert match");
  }
});

app.post("/matches/:id", upload.any(), async (req, res) => {
  let id = parseInt(req.params.id);
  console.log("id = ", id);

  if (Number.isNaN(id)) {
    res.status(400).end("Expected numeric id in params");
    return;
  }
  try {
    const result = await db.query(
      /*sql*/ `update matches 
      set
          match_time = $1, 
          match_home_team_name = $2, 
          match_home_team_score = $3, 
          match_away_team_name = $4, 
          match_away_team_score =$5, 
          match_id = $6, 
          match_venus = $7 
      where id = $8`,
      [
        req.body.match_time,
        req.body.match_home_team_name,
        req.body.match_home_team_score,
        req.body.match_away_team_name,
        req.body.match_away_team_score,
        req.body.match_id,
        req.body.match_venus,
        id,
      ]
    );
    // console.log('update result:', result)
    if (result.rowCount === 0) {
      res.status(404).end("Match not found");
    } else {
      res.status(202).end("Saved Match");
    }
  } catch (error) {
    console.log("failed to update match:", { error, id });

    res.status(500).end("failed to update match");
  }
});

app.delete("/matches/:id", async (req, res) => {
  let id = parseInt(req.params["id"]);
  if (Number.isNaN(id)) {
    res.status(400).end('expect number "id" in params');
    return;
  }
  try {
    await db.query(`delete from matches where id = $1`, [id]);
    res.end();
  } catch (error) {
    console.error("failed to delete match:", error);
    res.status(500).end("failed to delete match");
  }
});

app.post("/admin-form", upload.any(), async (req, res) => {
  console.log(req.body);
  console.log(req.body.input);

  (async () => {
    // await db.connect()

    let result = await db.query(`INSERT INTO team (team_info) VALUES ($1)`, [req.body.input]);
    console.log(result);

    await db.end();
  })();
  // res.redirect('/admin-form.html');
  res.send("done");
});

app.post("/register-form", upload.single("photo"), async (req, res) => {
  console.log(req.body, req.file);
  console.log("req.body.username:" + req.body.username);
  // console.log("req.body.username: " + req.body["username-input"]);  // undefined
  console.log("req.body.password:" + req.body.password);

  // console.log("req.body.password: " + req.body["password-input"]);  // undefined
  const userinfo = {
    username: req.body.username,
    password: req.body.password,
    filename: req.file.filename,
  };

  console.log("userinfo: ", userinfo);

  // await db.connect()

  let password = await bcrypt.hash(userinfo.password, 10);

  console.log(userinfo.username, password, userinfo.filename);

  let result = await db.query(`INSERT INTO users (username, password, user_icon) VALUES ($1, $2, $3)`, [
    userinfo.username,
    password,
    userinfo.filename,
  ]);
  console.log(result);

  // res.redirect('/');

  // await db.end()

  res.status(200).json("sucess");
});

app.post("/login-form", upload.any(), async (req, res) => {
  console.log("server: req.body.username: ", req.body.username);
  const users = (await db.query("SELECT * FROM users WHERE username = $1", [req.body.username])).rows;
  if (users.length == 0 || req.session == null) {
    res.status(403).send("Username invalid");
  } else {
    if (await bcrypt.compare(req.body.password, users[0].password)) {
      req.session["user"] = users[0];

      res.status(302).redirect("/");
      // res.status(302).send('ok');
    } else {
      res.status(403).send("Username or password invalid");
    }
  }
});

app.use((req, res, next) => {
  let date = new Date();
  let text = format(date, "YYYY-MM-DD hh:mm:ss");
  console.log(`[${text}]`, "Request", req.url);
  next();
});

app.get("/matches", async (req, res) => {
  try {
    const result = await db.query(
      /*sql*/ `select id,match_day,match_month,match_time,match_home_team_name,match_home_team_score,match_away_team_name,match_away_team_score,match_id,match_venus from matches order by match_id desc`
    );
    const matches: Match[] = result.rows;
    res.json(matches);
  } catch (error) {
    console.log("failed to get matches:", error);
    res.status(500).end("failed to get matches");
  }
});

app.get("/standingsPTS", async (req, res) => {
  let resultPTS = await db.query("Select * from NBAMatch order by PTS DESC LIMIT 5");
  res.json(resultPTS.rows).end();
});

app.get("/standingsTOT", async (req, res) => {
  let resultTOT = await db.query("Select * from NBAMatch order by TOT DESC LIMIT 5");
  res.json(resultTOT.rows).end();
});

app.get("/standingsA", async (req, res) => {
  let resultA = await db.query("Select * from NBAMatch order by A DESC LIMIT 5");
  res.json(resultA.rows).end();
});

app.get("/standings", async (req, res) => {
  let result = await db.query("Select * from NBAMatch order by PTS DESC LIMIT 5");
  let pts = result.rows;
  result = await db.query("Select * from NBAMatch order by TOT DESC LIMIT 5");
  let tot = result.rows;
  result = await db.query("Select * from NBAMatch order by A DESC LIMIT 5");
  let a = result.rows;
  res.json({ pts, tot, a });
});
app.use(express.static("public"));
app.use(express.static("uploads"));
app.use(userRoute);

//live + page socket
let counter = 0;

app.get("/counter", (req, res) => {
  res.json(counter);
});

app.post("/counter/inc", (req, res) => {
  counter++;
  io.emit("counter-updated", counter);
  //res.json(counter)
  res.end();
});

let counter2 = 0;

app.get("/counter2", (req, res) => {
  res.json(counter2);
});

app.post("/counter/inc2", (req, res) => {
  counter2++;
  io.emit("counter-updated2", counter2);
  //res.json(counter)
  res.end();
});

let teamAcount = 0;

app.get("/teamAcount", (req, res) => {
  res.json(teamAcount);
});

app.post("/teamAcount/inc", (req, res) => {
  teamAcount++;
  io.emit("teamAcount-updated", teamAcount);
  //res.json(counter)
  res.end();
});

app.post("/teamAcountless/inc", (req, res) => {
  if (teamAcount > 0) teamAcount--;
  io.emit("teamAcountless-updated", teamAcount);
  //res.json(counter)
  res.end();
});

let teamBcount = 0;

app.get("/teamBcount", (req, res) => {
  res.json(teamBcount);
});

app.post("/teamBcount/inc", (req, res) => {
  teamBcount++;
  io.emit("teamBcount-updated", teamBcount);
  //res.json(counter)
  res.end();
});

app.post("/teamBcountless/inc", (req, res) => {
  if (teamBcount > 0) teamBcount--;
  io.emit("teamBcountless-updated", teamBcount);
  //res.json(counter)
  res.end();
});

io.use((socket, next) => {
  let req = socket.request as express.Request;
  let res = req.res as express.Response;
  sessionMiddleware(req, res, next as express.NextFunction);
});

app.use((req, res) => {
  res.status(404);
  res.sendFile(path.join(__dirname, "public", "404.html"));
});

async function main() {
  try {
    console.log("connecting to database...");
    // await db.connect()
    console.log("connected to database");
  } catch (error) {
    console.log("failed to connect database:", error);
  }

  // let workbook = XLSX.readFile("NBA-Player-Sample-BoxScore-Dataset.xlsx");
  // let NBASheet = workbook.Sheets["NBA-SEASON-PLAYER-SAMPLE"];
  // let NBAMatches: NBAMatch[] = XLSX.utils.sheet_to_json(NBASheet);
  // for (let NBAMatch of NBAMatches) {
  //   // console.log(NBAMatch)
  //   await db.query(
  //     "insert into NBAMatch (PLAYERID, PLAYERFULLNAME, OWNTEAM, PTS, TOT, A) values ($1, $2, $3, $4 ,$5, $6)",
  //     [
  //       NBAMatch.PLAYERID,
  //       NBAMatch.PLAYERFULLNAME,
  //       NBAMatch.OWNTEAM,
  //       NBAMatch.PTS,
  //       NBAMatch.TOT,
  //       NBAMatch.A,
  //     ]
  //   );
  // }

  // const res = await db.query(
  //   "Select * from NBAMatch where PLAYERFULLNAME = $1",
  //   ["Luka Doncic"]
  // );
  // console.log(res.rows);
  // await db.end();
  server.listen(env.PORT, () => {
    console.log(`listening on http://localhost:${env.PORT}`);
  });
}

main();
