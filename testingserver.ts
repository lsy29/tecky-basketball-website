import express from "express";
import expressExpress from "express-session";
import path from "path";
import { format } from "fecha";
import jsonfile from "jsonfile";
import fs from "fs";
import multer from "multer";
import XLSX from 'xlsx'
import { db } from "./db";

let PORT = 4100;
let app = express();
let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./uploads");
  },
  filename: (req, file, cb) => {
    let filename = file.fieldname + "-" + Date.now() + "-" + file.originalname;
    cb(null, filename);
  },
});
let upload = multer({ storage });

type Match = {
  id: number;
  match_day: number;
  match_month: number;
  match_time: string;
  match_home_team_name: string;
  match_home_team_score: number;
  match_away_team_name: string;
  match_away_team_score: number;
  match_id: string;
  match_venus: string;
};

type NBAMatch = {
  PLAYERID: number
  PLAYERFULLNAME: string
  PTS: number
  TOT: number
  A: number
}

let matches: Match[] = [];

app.use(express.urlencoded({ extended: true }));

app.use(
  expressExpress({
    secret: "SECRET",
    saveUninitialized: true,
    resave: true,
  })
);

app.use((req, res, next) => {
  let counter = req.session["counter"] || 0;
  counter++;
  req.session["counter"] = counter;
  // console.log(counter);
  next();
});

app.use((req, res, next) => {
  let date = new Date();
  let text = format(date, "YYYY-MM-DD hh:mm:ss");
  console.log(`[${text}]`, "Request", req.url);
  next();
});

app.get("/matches", (req, res) => {
  res.json(matches);
});

app.post("/matches", upload.single("image"), (req, res) => {
  let match: Match = req.body;
  let maxId = matches.reduce((id, match) => {
    if (match.id > id) {
      return match.id
    }
    return id
  }, 0)
  match.id = maxId + 1

  matches.push(match);
  saveMatches(res).then(() => {
    res.status(201).end("created");
    // res.redirect(201, '/matches')
    // res.redirect('/matches')
  });
});

async function saveMatches(res: express.Response) {
  try {
    await jsonfile.writeFile("matches.json", matches, { spaces: 2 });
  } catch (error) {
    res.status(500).end("Failed to save matches");
  }
}

app.delete("/matches/:id", (req, res) => {
  let id = parseInt(req.params["id"]);
  if (Number.isNaN(id)) {
    res.status(400).end('expect number "id" in params');
    return;
  }
  matches = matches.filter((match) => match.id != id);
  saveMatches(res).then(() => {
    res.status(202).end("deleted");
  });
});

app.use(express.static("public"));

app.use((req, res) => {
  res.sendFile(path.join(__dirname, "public", "404.html"));
});

function loadJsonArray(file: string) {
  if (!fs.existsSync(file)) {
    fs.writeFileSync(file, "[]");
  }
  let array = jsonfile.readFileSync(file);
  for (let i = 0; i < array.length; i++) {
    let object = array[i];
    if (!object.id) {
      object.id = i + 1;
    }
  }
  return array;
}

async function main() {
  matches = loadJsonArray("matches.json");
  try {
    console.log('connecting to database...')
    // await db.connect()
    console.log('connected to database')
} catch (error) {
    console.log('failed to connect database:', error)
}

let workbook = XLSX.readFile('NBA-Player-Sample-BoxScore-Dataset.xlsx')
let NBASheet = workbook.Sheets['NBA-SEASON-PLAYER-SAMPLE']
let NBAMatches: NBAMatch[] = XLSX.utils.sheet_to_json(NBASheet)
for (let NBAMatch of NBAMatches) {
    // console.log(NBAMatch)
    await db.query(
        'insert into NBAMatch (PLAYERID, PLAYERFULLNAME, PTS, TOT, A) values ($1, $2, $3, $4 ,$5)',
        [NBAMatch.PLAYERID, NBAMatch.PLAYERFULLNAME, NBAMatch.PTS, NBAMatch.TOT, NBAMatch.A],
    )
}

const res = await db.query('Select * from NBAMatch where PLAYERFULLNAME = $1', ['Luka Doncic'])
console.log(res.rows);
await db.end()
  app.listen(PORT, () => {
    console.log(`listening on http://localhost:${PORT}`);
  });
}

main();
