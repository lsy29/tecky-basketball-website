import {Request,Response,NextFunction} from 'express';

export function isLoggedIn(req:Request,res:Response,next:NextFunction){
    if(req.session?.['user']){
        // res.send("true")
        next();
    }else{
        res.redirect('/login.html');
    }
}

export function isAdmin(req:Request,res:Response,next:NextFunction){
    if(req.session?.['user']){
        res.send("true")
        next();
    }else{
        res.redirect('/login.html');
    }
}