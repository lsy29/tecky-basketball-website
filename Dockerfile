FROM node:lts
WORKDIR /usr/src/app
COPY . .
EXPOSE 8100
CMD yarn install && \
    ts-node server.ts