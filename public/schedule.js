let photos = {
  南華: `/photos/BallTeamsLogo/SCAA南華.jpeg`,
  永倫: `/photos/BallTeamsLogo/Winling永倫.jpeg`,
  東方: `/photos/BallTeamsLogo/Eastern_Sports東方.png`,
  福建: `/photos/BallTeamsLogo/FUKIEN福建籃球隊.jpeg`,
  飛鷹: `/photos/BallTeamsLogo/EAGLE飛鷹籃球隊.jpeg`,
  遊協: `/photos/BallTeamsLogo/HKPA香港遊樂場協會籃球隊.png`,
  滿貫: `/photos/BallTeamsLogo/Tycoon滿貫.png`,
  南青: `/photos/BallTeamsLogo/NC南青籃球隊.png`,
  建龍飛馬: `/photos/BallTeamsLogo/PK建龍飛馬籃球隊.jpeg`,
  漢友: `/photos/BallTeamsLogo/Ble漢友.jpeg`,
};

let matchesDiv = document.querySelector("#matches");
matchesDiv.textContent = `loading ...`;

// console.log("loadMatches");

async function loadMatches() {
  let res = await fetch("/matches");
  let matches = await res.json();

  matchesDiv.innerHTML = "";
  for (let match of matches) {
    // console.log(photos[match.match_away_team_name]);
    let m = moment(`2021-${match.match_month}-${match.match_day}`);
    matchesDiv.innerHTML += `
    <div class="btn offset-md-10" style="font-size: -webkit-xxx-large">
    <i class="fas fa-edit" id="edit-button${match.id}" onclick='editMatch(${
      match.id
    })'></i>
    <i class="fas fa-trash-alt" onclick="deleteMatch(${match.id})"></i>
    <i class="fas fa-check-circle" id="end-editing${
      match.id
    }" onclick='doneEditMatch(${match.id})'></i>
    </div>
    <form id="update-schedule${match.id}" method="POST" action="/matches${match.id}">
    <div class="row game-record">
    <div class="col-1 g-date">
        <div id="MatchDnM${match.id} class="t1">${match.match_day}/${match.match_month}</div>
        <div>(${m.format("ddd")})</div>
        <div id="timeID${match.id}">${match.match_time}</div>
    </div>
    <div class="col-2 team-a">
        <div>客隊</div>
        <div id="awayTeam${match.id}">${match.match_away_team_name}</div>
    </div>
    <div class="col-1 logo-flex">
        <div class="logo-box">
            <img id="awayTeamImage${match.id}" src="${
      photos[match.match_away_team_name]
    }" height="100%">
        </div>
    </div>
  
    <div class="col-1 team-a">
      <div class="t1" id="awayTeamScore${match.id}">${
      match.match_away_team_score
    }</div>
    </div>
    <div class="col-2 game-info">
        <div id="matchID${match.id}">常規賽${match.match_id}</div>
        <div id="venue${match.id}">${match.match_venus}</div>
    </div>
    <div class="col-1 team-b">
        <div class="t1" id="homeTeamScore${match.id}">${
      match.match_home_team_score
    }</div>
    </div>
    <div class="col-1 logo-flex">
        <div class="logo-box">
            <img id="homeTeamImage${match.id}" src="${
      photos[match.match_home_team_name]
    }" height="100%">
        </div>
    </div>
    <div class="col-2 team-b">
        <div>主隊</div>
        <div id="homeTeam${match.id}">${match.match_home_team_name}</div>
    </div>
    <div class="col-1 game-info">
        <a href="/game-record-date.html">
            <div>數據 <i class="fas fa-chevron-right"></i></div>
        </a>
        <a href="/#">
            <div>售票 <i class="fas fa-chevron-right"></i></div>
        </a>
  
    </div>
  
  </div></form>`;
  }
}
loadMatches();

function editMatch(scheduleID) {
  let timeID = document.querySelector(`#timeID${scheduleID}`);
  timeID.innerHTML = `<input type="time" id="match_time${scheduleID}" name="match_time" />`;

  let matchID = document.querySelector(`#matchID${scheduleID}`);
  matchID.innerHTML = `<input
  type="number"
  id="match_id${scheduleID}"
  name="match_id"
  placeholder="球賽編號"
/>`;

  let awayTeamScore = document.querySelector(`#awayTeamScore${scheduleID}`);

  awayTeamScore.innerHTML = `<input
  type="number"
  id="match_away_team_score${scheduleID}"
  name="match_away_team_score"
  placeholder="客隊分數"
  min="0"
  max="999"
/>`;

  let homeTeamScore = document.querySelector(`#homeTeamScore${scheduleID}`);

  homeTeamScore.innerHTML = `<input
type="number"
id="match_home_team_score${scheduleID}"
name="match_home_team_score"
placeholder="主隊分數"
min="0"
max="999"
/>`;

  let venue = document.querySelector(`#venue${scheduleID}`);
  venue.innerHTML = `
<select id="match_venue${scheduleID}" name="match_venue">
<option disabled selected=""></option>
<option value="青衣體育館">青衣體育館</option>
<option value="荃灣體育館">荃灣體育館</option>
<option value="北河街體育館">北河街體育館</option>
<option value="石硤尾公園體育館">
  石硤尾公園體育館
</option>
<option value="荔枝角公園體育館">
  荔枝角公園體育館
</option>
<option value="修頓體育館">修頓體育館</option>
<option value="石塘咀體育館">石塘咀體育館</option>
<option value="士美菲路體育館">士美菲路體育館</option>
<option value="伊利沙伯體育館">伊利沙伯體育館</option>
<option value="中山紀念公園體育館">
  中山紀念公園體育館
</option>
</select>`;

  let homeTeam = document.querySelector(`#homeTeam${scheduleID}`);
  homeTeam.innerHTML = `<select
id="match_home_team_name${scheduleID}"
name="match_home_team_name"
onchange="changeFlipHomeIMG(${scheduleID})"
>
<option disabled selected=""></option>
<option
  url="/photos/BallTeamsLogo/SCAA南華.jpeg"
  value="南華"
>
  南華
</option>
<option
  url="/photos/BallTeamsLogo/Winling永倫.jpeg"
  value="永倫"
>
  永倫
</option>
<option
  url="/photos/BallTeamsLogo/Eastern_Sports東方.png"
  value="東方"
>
  東方
</option>
<option
  url="/photos/BallTeamsLogo/FUKIEN福建籃球隊.jpeg"
  value="福建"
>
  福建
</option>
<option
  url="/photos/BallTeamsLogo/EAGLE飛鷹籃球隊.jpeg"
  value="飛鷹"
>
  飛鷹
</option>
<option
  url="/photos/BallTeamsLogo/HKPA香港遊樂場協會籃球隊.png"
  value="遊協"
>
  遊協
</option>
<option
  url="/photos/BallTeamsLogo/Tycoon滿貫.png"
  value="滿貫"
>
  滿貫
</option>
<option
  url="/photos/BallTeamsLogo/NC南青籃球隊.png"
  value="南青"
>
  南青
</option>
<option
  url="/photos/BallTeamsLogo/PK建龍飛馬籃球隊.jpeg"
  value="建龍飛馬"
>
  建龍飛馬
</option>
<option
  url="/photos/BallTeamsLogo/Ble漢友.jpeg"
  value="漢友"
>
  漢友
</option>
</select>`;
  let awayTeam = document.querySelector(`#awayTeam${scheduleID}`);
  awayTeam.innerHTML = `                      <select
  id="match_away_team_name${scheduleID}"
  name="match_away_team_name"
  onchange="changeFlipAwayIMG(${scheduleID})"
>
  <option disabled selected=""></option>
  <option
    url="/photos/BallTeamsLogo/SCAA南華.jpeg"
    value="南華"
  >
    南華
  </option>
  <option
    url="/photos/BallTeamsLogo/Winling永倫.jpeg"
    value="永倫"
  >
    永倫
  </option>
  <option
    url="/photos/BallTeamsLogo/Eastern_Sports東方.png"
    value="東方"
  >
    東方
  </option>
  <option
    url="/photos/BallTeamsLogo/FUKIEN福建籃球隊.jpeg"
    value="福建"
  >
    福建
  </option>
  <option
    url="/photos/BallTeamsLogo/EAGLE飛鷹籃球隊.jpeg"
    value="飛鷹"
  >
    飛鷹
  </option>
  <option
    url="/photos/BallTeamsLogo/HKPA香港遊樂場協會籃球隊.png"
    value="遊協"
  >
    遊協
  </option>
  <option
    url="/photos/BallTeamsLogo/Tycoon滿貫.png"
    value="滿貫"
  >
    滿貫
  </option>
  <option
    url="/photos/BallTeamsLogo/NC南青籃球隊.png"
    value="南青"
  >
    南青
  </option>
  <option
    url="/photos/BallTeamsLogo/PK建龍飛馬籃球隊.jpeg"
    value="建龍飛馬"
  >
    建龍飛馬
  </option>
  <option
    url="/photos/BallTeamsLogo/Ble漢友.jpeg"
    value="漢友"
  >
    漢友
  </option>
</select>`;
}

async function doneEditMatch(scheduleID) {
  console.log(scheduleID);

  let updateMatchForm = document.querySelector(`#update-schedule${scheduleID}`);
  // updateMatchForm.addEventListener("submit", async (event) => {
    // event.preventDefault();
    let formData = new FormData(updateMatchForm);
    let res = await fetch(`/matches/${scheduleID}`, {
      method: "POST",
      body: formData,
    });
    let message = await res.text();
    if (message === "created") {
      updateMatchForm.reset();
      loadMatches();
    } else {
      alert(message);
    }
//   // });

  let timeID = document.querySelector(`#timeID${scheduleID}`);
  let changeTimeID = document.querySelector(`#match_time${scheduleID}`).value;
  timeID.innerHTML = `${changeTimeID}`;

  let matchID = document.querySelector(`#matchID${scheduleID}`);
  let changeMatchID = document.querySelector(`#match_id${scheduleID}`).value;
  matchID.innerHTML = `常規賽${changeMatchID}`;

  let awayTeamScore = document.querySelector(`#awayTeamScore${scheduleID}`);
  let awayScore = document.querySelector(`#match_away_team_score${scheduleID}`)
    .value;
  awayTeamScore.innerHTML = `${awayScore}`;

  let homeTeamScore = document.querySelector(`#homeTeamScore${scheduleID}`);
  let homeScore = document.querySelector(`#match_home_team_score${scheduleID}`)
    .value;
  homeTeamScore.innerHTML = `${homeScore}`;

  let venue = document.querySelector(`#venue${scheduleID}`);
  let changeVenue = document.querySelector(`#match_venue${scheduleID}`).value;
  venue.innerHTML = `${changeVenue}`;

  let homeTeam = document.querySelector(`#homeTeam${scheduleID}`);
  let changeHomeTeam = document.querySelector(
    `#match_home_team_name${scheduleID}`
  ).value;
  homeTeam.innerHTML = `${changeHomeTeam}`;

  let awayTeam = document.querySelector(`#awayTeam${scheduleID}`);
  let changeAwayTeam = document.querySelector(
    `#match_away_team_name${scheduleID}`
  ).value;
  awayTeam.innerHTML = `${changeAwayTeam}`;

  console.log("Send update");


}

async function deleteMatch(id) {
  Swal.fire({
    title: "Are you sure?",
    text: "You won't be able to revert this!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Yes, delete it!",
  }).then(async (result) => {
    console.log(result);
    if (result.isConfirmed) {
      Swal.fire("Deleted!", "Your file has been deleted.", "success");
      let res = await fetch("/matches/" + id, {
        method: "DELETE",
      });
      console.log("ABDDDD");
      loadMatches();
    }
  })
}

let postMatchForm = document.querySelector("#post-schedule");
postMatchForm.addEventListener("submit", async (event) => {
  event.preventDefault();
  let formData = new FormData(postMatchForm);
  let res = await fetch("/matches", {
    method: "POST",
    body: formData,
  });
  let message = await res.text();
  if (message === "created") {
    postMatchForm.reset();
    loadMatches();
  } else {
    alert(message);
  }
});

function flipAwayIMG() {
  let match_away_team_img = document.getElementById("match_away_team_img");
  let match_away_team_name = document.getElementById("match_away_team_name")
    .value;
  match_away_team_img.src = photos[match_away_team_name];
}

function flipHomeIMG() {
  let match_home_team_img = document.getElementById("match_home_team_img");
  let match_home_team_name = document.getElementById("match_home_team_name")
    .value;
  match_home_team_img.src = photos[match_home_team_name];
}

function changeFlipAwayIMG(scheduleID) {
  let match_away_team_img = document.getElementById(
    `awayTeamImage${scheduleID}`
  );
  let match_away_team_name = document.getElementById(
    `match_away_team_name${scheduleID}`
  ).value;
  match_away_team_img.src = photos[match_away_team_name];
}

function changeFlipHomeIMG(scheduleID) {
  let match_home_team_img = document.getElementById(
    `homeTeamImage${scheduleID}`
  );
  let match_home_team_name = document.getElementById(
    `match_home_team_name${scheduleID}`
  ).value;
  match_home_team_img.src = photos[match_home_team_name];
}


let schedule = document.querySelector('#form-container')
let adminBTN = document.querySelector('#matches')

// window.checkRole = checkRole;
async function checkRole() {
  // console.log('check role is running...');  
  let res = await fetch('/role')
  let role = await res.text()

  // console.log("role: ", role);
  if (role === 'admin') {
    schedule.classList.add('admin')
    adminBTN.classList.add('admin')
    // console.log("from checkRole(): admin class added");
    // console.log("admin login");
  //   setTimeout(function(){
  //     window.location.href = '/admin-form.html';
  // }, 3000);
    // loginForm.style.display = 'none'
    // //logoutForm.style.display = 'block'
  } else {
    schedule.classList.remove('admin')
    console.log("from checkRole(): admin class removed");
  //   setTimeout(function(){
  //     window.location.href = '/';
  // }, 3000);
    // adminRight.style.display = 'none'
    setTimeout(function(){
      // window.location.href = '/';
  }, 1500);
    // loginForm.style.display = 'block'
    // logoutForm.style.display = 'none'
    
  }
}

checkRole();
