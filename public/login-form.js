let loginForm = document.querySelector('#login-form')

// console.log("login-form.js");
console.log(loginForm);
// console.log("schdule: ", );
loginForm.addEventListener('submit', async event => {
  console.log("login form ajax is running");
  event.preventDefault()
  let formData = {
    username: loginForm.username.value,
    password: loginForm.password.value,
  }
  console.log("sending out :", formData);
  let res = await fetch('/login-form', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(formData),
  })
  console.log('login res:', res)
  if (res.status === 200) {
      // res.redirect('/admin-form')
      const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 1500,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })
    
    Toast.fire({
      icon: 'success',
      title: 'Login Success'
    })
    if(typeof loadMatches == "function"){
      loadMatches()
    }
      setTimeout(function(){
        // window.location.href = '/';
        window.location.reload()
    }, 1500);
    // let message = document.querySelector('#login-message')
    // message.innerHTML = "Login Sucess!"
    // message.style.color = "green"
    // setTimeout(function(){
    //     message.innerHTML = '';
    // }, 1500);
    // alert("Login success!"); 
    console.log("now calling the check role function");
    checkRole()
  }else {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 1500,
      timerProgressBar: true,
      didOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
      }
    })
    
    Toast.fire({
      icon: 'false',
      title: 'Register false'
    })
    // let message = document.querySelector('#login-message')
    // message.innerHTML = "Username or password invalid!"
    // message.style.color = "red"
    setTimeout(function(){
        document.querySelector('#login-username-input').innerHTML = '';
        // document.querySelector('#login-password-input').innerHTML = '';
    }, 1500);
    setTimeout(function(){
        message.innerHTML = '';
    }, 1500);
  }
})

let schedule = document.querySelector('#form-container')
// window.checkRole = checkRole;
async function checkRole() {
  console.log('check role is running...');  
  let res = await fetch('/role')
  let role = await res.text()

  console.log("role: ", role);
  if (role === 'admin') {
    document.body.classList.add('admin')
    console.log("from checkRole(): admin class added");
    console.log("admin login");
  //   setTimeout(function(){
  //     window.location.href = '/admin-form.html';
  // }, 1500);
    // loginForm.style.display = 'none'
    // //logoutForm.style.display = 'block'
  } else {
    document.body.classList.remove('admin')
    console.log("from checkRole(): admin class removed");
  //   setTimeout(function(){
  //     window.location.href = '/';
  // }, 1500);
    // adminRight.style.display = 'none'
    setTimeout(function(){
      // window.location.href = '/';
  }, 1500);
    // loginForm.style.display = 'block'
    // logoutForm.style.display = 'none'
    
  }
}

checkRole();