// var big_image;

const userProfileAccess = document.querySelector("#userProfile");
const userIcon = document.querySelector("#user_icon");
const userName = document.querySelector("#user_name");
const userEmail = document.querySelector('#user_email');
const userNameForm = document.querySelector("#userprofile-username-form");
const userEmailForm = document.querySelector("#userprofile-email-form");


async function userProfile() {
    let res = await fetch("/user-profile");
    let content = await res.json();
    console.log(content);
    console.log("content.email: ", content.email);
    
    if (content.username != null){
      userName.innerHTML = '';
      userName.innerText = `${content.username}`;
      userNameForm.style.display = 'none'
    } else if (content.username = null) {
      userNameForm.style.display = 'block'

      usernameRegister()
    }

    if (content.email != null) {
      userEmail.innerText = `Email: ${content.email}`;
      userEmailForm.style.display = 'none'
    } else if (content.email = null) {
      userEmailForm.style.display = 'block'


      console.log("waiting for user to submit email");

      userEmailRegister()
    }
   

    if (content.user_icon != null) {
      userIcon.src = content.user_icon

    } else if (content.user_icon = null){
      userIcon.src = "/icone-homem-man-icon-11553494437bgj8eja2bq.png";
    }
        
}

userProfile()

// readEmail();


  async function usernameRegister() {
  console.log("usernameRegister is running...");
  userNameForm.addEventListener("submit", async event => {
    event.preventDefault()

    let formData = {
      username: userNameForm.usernameName.value,
    }

    console.log("sending out: ", formData);

    let res = await fetch('/userprofile-register-username', {
      method: "POST", 
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formData)
    })

    if (await res.status === 200) {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 1500,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        },
        didDestroy: ()=>{
          // readEmail()
          // location.reload()
          userProfile()
        }

      })
      
      Toast.fire({
        icon: 'success',
        title: 'Username Updated'
      })
  }

})

}




  async function userEmailRegister() {
  console.log("userEmailRegister is running...");
  userEmailForm.addEventListener("submit", async event => {
    event.preventDefault()

    let formData = {
      email: userEmailForm.emailName.value,
    }

    console.log("sending out: ", formData);

    let res = await fetch('/userprofile-register-email', {
      method: "POST", 
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formData)
    })

    if (await res.status === 200) {
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 1500,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        },
        didDestroy: ()=>{
          // readEmail()
          // location.reload()
          userProfile()
        }
      })
      
      Toast.fire({
        icon: 'success',
        title: 'Email Updated'
      })
    }

    console.log("call readEmail() inside userEmailRegister()");
    console.log('data after stringify: ', res);

  })

}


userEmailRegister()


usernameRegister()






