// console.log("load header.js");

async function loadHeader() {
    let res = await fetch('/role')
    let role = await res.text()
    let html = /* html */`
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark  fixed-top " aria-label="Ninth navbar example">
    <div class="container-xl">
        <a class="navbar-brand" href="/index.html"><img width="40px" src="/photos/HkbaLogo.png">
            HKBA
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample07XL"
            aria-controls="navbarsExample07XL" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
  
        <div class="collapse navbar-collapse" id="navbarsExample07XL">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link" aria-current="page" href="/index.html">主頁</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/schedule.html">賽程</a>
                </li>
  
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="/standings.html" id="dropdown07XL"
                        data-bs-toggle="dropdown" aria-expanded="false">數據排行榜</a>
                    <ul class="dropdown-menu" aria-labelledby="dropdown07XL">
                        <li><a class="dropdown-item" href="/standings.html">綜合排行</a></li>
                        <li><a class="dropdown-item" href="#">球隊排行</a></li>
                        <li><a class="dropdown-item" href="#">球員數據排行</a></li>
                    </ul>
                </li>
  
                <li class="nav-item">
                    <a class="nav-link" href="/team-page.html">甲一球隊</a>
                </li>
  
                <li class="nav-item">
                    <a class="nav-link" href="/news.html">聯賽消息/公告</a>
                </li>
  
                <li class="nav-item">
                    <a class="nav-link" href="live-page.html">球賽直播</a>
                </li>
  
                ${role === 'admin' ? `<li class="nav-item">
                    <a class="nav-link" href="/admin-form.html" id='admin-rights' >Admin Panel</a>
                </li>` : ``} 
  
<!--                <li class="nav-item">
                    <a class="nav-link" href="/login-system-test.html">Login System Test</a>
                </li>     -->
  
  
            </ul>
            <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button" data-bs-toggle="modal" data-bs-target="#exampleModal">
                <i class="fas fa-user" ></i></a>
  
            <a class="btn btn-outline-light btn-floating m-1" aria-hidden="true" id="logoutBTN">
                <i class="fas fa-sign-out-alt" ></i></a>

            <a class="btn btn-outline-light btn-floating m-1" href="/user-profile.html" aria-hidden="true" id="userProfile" onclick="userInfoCheck() ">
            <i class="fas fa-user-circle" ></i></a>
        </div>
    </div>
  </nav>
  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Sign In</h5>
      <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <div class="modal-body">
        <a href="/connect/google">Login with Google</a><br><br>
<!--        <input type="text" placeholder="Username"/><br><br>
        <input type="password" placeholder="Password"/>
        <br><br>
        <button type="button" class="btn btn-primary">Sign in</button>
        <br>    -->
            <form id="login-form" class="login-form-contrainer" action="/login-form" method="POST" enctype="multipart/form-data">
            
                <input type="text" id="login-username-input" class="username-input" name="username" placeholder="Username">
                <p></p>
                <input type="password" id="login-password-input" class="password-input" name="password" placeholder="Password">
                <p></p>
                <div id="login-message" style="color:red;"></div>
                <input type="submit" name="" id="" value="Login">
            </form>
        <p></p>
        <a href="register-system-test.html">New to HKBA? Register</a>
        <p></p>
        <a href="logout-form.html">Logout</a> 
    </div>
    <div class="modal-footer">
  <p>By submitting this form you agree to HKBA's Privacy Policy and Terms of Use.
  If you have any issues, please visit the Support Center for assistance.</p>
    </div>
  </div>
  </div>
  </div>
  <script src=/logout-form.js id='loadLogoutFormjs'></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@10" id='loadSweetAlert'></script>
  <script src=/login-form.js id='loadLoginFormjs'></script>
  `

    document.querySelector('header').innerHTML = html

    res = await fetch("/login-form.js")
    let loginFormjs = await res.text()
    // console.log("js: ", loginFormjs);
    document.querySelector('#loadLoginFormjs').textContent = loginFormjs
    eval(loginFormjs)

    res = await fetch("/logout-form.js")
    let logooutFormjs = await res.text()
    document.querySelector('#loadLogoutFormjs').textContent = logooutFormjs
    eval(logooutFormjs)

    res = await fetch("//cdn.jsdelivr.net/npm/sweetalert2@10")
    let sweetalert = await res.text()
    document.querySelector('#loadSweetAlert').textContent = sweetalert
    eval(sweetalert)



    let loginForm = document.querySelector('#login-form')
    // console.log("check login form", loginForm);

    document.querySelector('#logoutBTN').addEventListener('click', async event => {
        event.preventDefault()
        console.log('button clicked');
        console.log(document.querySelector('#logoutBTN'))
        let res = await fetch('/logout', {
            method: "POST"
        });
        if (res.status === 200) {
            window.location.href = '/';
        } else {
            window.location.href = '/login-system-test.html';
        }
        console.log(await res.json())
    })
}

loadHeader()

