document.querySelector('#add-team').addEventListener('submit', async event => {   
    event.preventDefault()    

    // trans HTML form into FormData  <-- multipart/form-data 
    const inputValue = new FormData();
    // const inputValue = new FormData(document.querySelector('#team-info-input').value);
    inputValue.append("input", document.querySelector('#team-info-input').value)

    // upload the team info to DB
    let res = await fetch('/admin-form', {
        method: 'POST',
        body: inputValue
        // headers : { 
        //     'Content-Type': 'application/json',
        //     'Accept': 'application/json'
        //    }
    })
    console.log(await res)
    window.location.href = '/'
})