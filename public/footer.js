let footer = document.querySelector('footer')
footer.innerHTML = /* html */ `

    <!-- Footer -->
    <footer class="bg-dark text-center text-white">
        <!-- Grid container -->
        <div class="container p-4">
            <p>關注我們</p>
            <!-- Section: Social media -->
            <section class="mb-4">
                <!-- Facebook -->
                <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"><i
                        class="fab fa-facebook-f"></i></a>

                <!-- Twitter -->
                <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"><i
                        class="fab fa-twitter"></i></a>

                <!-- Instagram -->
                <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"><i
                        class="fab fa-instagram"></i></a>

                <!-- Youtube -->
                <a class="btn btn-outline-light btn-floating m-1" href="#!" role="button"><i
                        class="fab fa-youtube"></i></a>



            </section>
            <!-- Section: Social media -->

            <!-- Section: Form -->
            <section class="">
                <form action="">
                    <!--Grid row-->
                    <div class="row d-flex justify-content-center">
                        <!--Grid column-->
                        <div class="col-auto">
                            <p class="pt-2">
                                <strong>電子報訂閱</strong>
                            </p>
                        </div>
                        <!--Grid column-->

                        <!--Grid column-->
                        <div class="col-md-5 col-12">
                            <!-- Email input -->
                            <div class="form-outline form-white mb-4">
                                <input type="email" id="form5Example2" class="form-control" placeholder="Your Email" />

                            </div>
                        </div>
                        <!--Grid column-->

                        <!--Grid column-->
                        <div class="col-auto">
                            <!-- Submit button -->
                            <button type="submit" class="btn btn-outline-light mb-4">
                                訂閱
                            </button>
                        </div>
                        <!--Grid column-->
                    </div>
                    <!--Grid row-->
                </form>
            </section>
            <!-- Section: Form -->

            <!-- Section: Text -->
            <div class="text-nowrap mb-2">
                <p class="fs12 mb-4">
                    <a class="text-light" href="#">媒體合作</a>｜
                    <a class="text-light" href="#">業務合作</a>｜
                    <a class="text-light" href="#">購物須知</a>｜
                    <a class="text-light" href="#">服務條款</a>｜
                    <a class="text-light" href="#">隱私權政策</a>
                </p>
                <p>香港籃球總會有限公司 Hong Kong Basketball Association Limited
                    <br>
                    香港銅鑼灣掃桿埔大球場徑1號奧運大樓1006室
                </p>
                <p class="text-white fs14 my-3">Copyright © 2021 版權所有 不得轉載</p>
            </div>
    </footer>
    <!-- Footer -->`