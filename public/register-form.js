document.querySelector('#register-form').addEventListener('submit', async event => {
    event.preventDefault()
    console.log(document.querySelector('#register-form'))
    // trans HTML form into FormData  <-- multipart/form-data  
    const inputValue = new FormData();
    // const inputValue = new FormData(document.querySelector('#register-form'));

    // const inputValue = new FormData(document.querySelector('#team-info-input').value);
    inputValue.append("username", document.querySelector('#register-username-input').value)
    inputValue.append("password", document.querySelector('#register-password-input').value)
    inputValue.append("photo", document.querySelector('#icon-upload').files[0])

    // upload the team info to DB
    let res = await fetch('/register-form', {
        method: 'POST',
        body: inputValue
    })
    if (res.status === 200) {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 1500,
            timerProgressBar: true,
            didOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer)
              toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
          })
          
          Toast.fire({
            icon: 'success',
            title: 'Register Success'
          })
          setTimeout(function(){
            window.location.href = '/';
        }, 1500);
        // let message = document.querySelector('#register-message')
        // message.innerHTML = "Login Sucess!"
        // message.style.color = "green"
        // setTimeout(function(){
        //     message.innerHTML = '';
        // }, 1500);
      } else {
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 1500,
          timerProgressBar: true,
          didOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
          }
        })
        
        Toast.fire({
          icon: 'false',
          title: 'Register false'
        })
        //  res.status(403).send('Username invalid') 
      }
    

    
    console.log(await res.json())
})