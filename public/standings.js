let ptsRankDiv = document.querySelector("#pts0");
ptsRankDiv.textContent = `loading ...`;
let totRankDiv = document.querySelector("#tot0");
totRankDiv.textContent = `loading ...`;
let aRankDiv = document.querySelector("#a0");
aRankDiv.textContent = `loading ...`;

///for PTS ranking///
async function rankingPTS() {
  let res = await fetch("/standingsPTS");
  let top5PlayersPTS = await res.json();
  ptsRankDiv.innerHTML = "";
  for (playerPTS = 0; playerPTS < 5; playerPTS++) {
    ptsRankDiv.innerHTML += `<tr class="dark_gradient">
    <th class="text-center px-md-3 px-2 fs14" scope="row" style="min-width:50px;"><i>${
      top5PlayersPTS.indexOf(top5PlayersPTS[playerPTS]) + 1
    }</i></th>
    <td id="pts0" class="pl-1 pr-0" style="min-width:115px;"><a  class="fs14 font-weight-bold" href="/player/43">${
      top5PlayersPTS[playerPTS].playerfullname
    }</a><a class="fs12 d-block text-secondary opacity-4" href="/team/4">${
      top5PlayersPTS[playerPTS].ownteam
    }</a></td>
    <td class="text-light fs14 "><i>${
      top5PlayersPTS[playerPTS].pts
    }</i></td></tr>`;
  }
}
rankingPTS();
///for PTS ranking///

///for TOT ranking///
async function rankingTOT() {
  let res = await fetch("/standingsTOT");
  let top5PlayersTOT = await res.json();
  totRankDiv.innerHTML = "";
  for (playerTOT = 0; playerTOT < 5; playerTOT++) {
    totRankDiv.innerHTML += `            <tr class="dark_gradient">
    <th class="text-center px-md-3 px-2 fs14" scope="row" style="min-width:50px;"><i>${
      top5PlayersTOT.indexOf(top5PlayersTOT[playerTOT]) + 1
    }</i></th>
    <td class="pl-1 pr-0" style="min-width:115px;"><a class="fs14 font-weight-bold " href="/player/68">${
      top5PlayersTOT[playerTOT].playerfullname
    }</a><a class="fs12 d-block text-secondary opacity-4" href="/team/3">${
      top5PlayersTOT[playerTOT].ownteam
    }</a></td>
    <td class="text-light fs14 "><i>${
      top5PlayersTOT[playerTOT].tot
    }</i></td></tr>`;
  }
}
rankingTOT();
///for TOT ranking///

///for A ranking///
async function rankingA() {
  let res = await fetch("/standingsA");
  let top5PlayersA = await res.json();
  aRankDiv.innerHTML = "";
  for (playerA = 0; playerA < 5; playerA++) {
    aRankDiv.innerHTML += `<tr class="dark_gradient">
    <th class="text-center px-md-3 px-2 fs14" scope="row" style="min-width:50px;"><i>${
      top5PlayersA.indexOf(top5PlayersA[playerA]) + 1
    }</i></th>
    <td class="pl-1 pr-0" style="min-width:115px;"><a class="fs14 font-weight-bold " href="/player/43">${
      top5PlayersA[playerA].playerfullname
    }</a><a class="fs12 d-block text-secondary opacity-4" href="/team/4">${
      top5PlayersA[playerA].ownteam
    }</a></td>
    <td class="text-light fs14 "><i>${
      top5PlayersA[playerA].a
    }</i></td>
    </tr>`;
  }
}
rankingA();
///for A ranking///
