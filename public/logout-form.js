document.querySelector('#logoutBTN').addEventListener('click', async event => {
    event.preventDefault()
    // console.log('button clicked');       
    // console.log(document.querySelector('#logoutBTN'))
    let res = await fetch('/logout', {
        method: "POST"
    });
    if (res.status === 200) {
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
              toast.addEventListener('mouseenter', Swal.stopTimer)
              toast.addEventListener('mouseleave', Swal.resumeTimer)
            },
              didDestroy: ()=>{
              // readEmail()
              location.reload()
              }
          })
          
          Toast.fire({
            icon: 'success',
            title: 'Logout Success'
          })
      } 

    //   setTimeout(function(){
    //     window.location.href = '/';
    // }, 1500);
    // else {
    //     window.location.href = '/'; 
    // }
    console.log(await res.json())
})
